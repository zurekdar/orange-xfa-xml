﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frm_XfaToDs
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.panel_Main = New System.Windows.Forms.Panel()
        Me.cmd_Select = New System.Windows.Forms.Button()
        Me.txt_Dataset = New System.Windows.Forms.TextBox()
        Me.panel_Main.SuspendLayout()
        Me.SuspendLayout()
        '
        'panel_Main
        '
        Me.panel_Main.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.panel_Main.Controls.Add(Me.txt_Dataset)
        Me.panel_Main.Controls.Add(Me.cmd_Select)
        Me.panel_Main.Dock = System.Windows.Forms.DockStyle.Fill
        Me.panel_Main.ForeColor = System.Drawing.SystemColors.ControlText
        Me.panel_Main.Location = New System.Drawing.Point(0, 0)
        Me.panel_Main.Name = "panel_Main"
        Me.panel_Main.Size = New System.Drawing.Size(642, 320)
        Me.panel_Main.TabIndex = 0
        '
        'cmd_Select
        '
        Me.cmd_Select.Location = New System.Drawing.Point(12, 12)
        Me.cmd_Select.Name = "cmd_Select"
        Me.cmd_Select.Size = New System.Drawing.Size(154, 23)
        Me.cmd_Select.TabIndex = 0
        Me.cmd_Select.Text = "Wybierz dokument PDF"
        Me.cmd_Select.UseVisualStyleBackColor = True
        '
        'txt_Dataset
        '
        Me.txt_Dataset.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.txt_Dataset.BackColor = System.Drawing.Color.White
        Me.txt_Dataset.Location = New System.Drawing.Point(12, 42)
        Me.txt_Dataset.Multiline = True
        Me.txt_Dataset.Name = "txt_Dataset"
        Me.txt_Dataset.ReadOnly = True
        Me.txt_Dataset.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me.txt_Dataset.Size = New System.Drawing.Size(617, 265)
        Me.txt_Dataset.TabIndex = 1
        Me.txt_Dataset.WordWrap = False
        '
        'frm_XfaToDs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(642, 320)
        Me.Controls.Add(Me.panel_Main)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.MaximizeBox = False
        Me.Name = "frm_XfaToDs"
        Me.Text = "Xfa To Dataset"
        Me.panel_Main.ResumeLayout(False)
        Me.panel_Main.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents panel_Main As Panel
    Friend WithEvents txt_Dataset As TextBox
    Friend WithEvents cmd_Select As Button
End Class
