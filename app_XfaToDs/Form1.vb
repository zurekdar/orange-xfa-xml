﻿Imports System.Xml
Imports iTextSharp.text.pdf

Public Class frm_XfaToDs
    Private Sub cmd_Select_Click(sender As Object, e As EventArgs) Handles cmd_Select.Click
        Dim exe_Path As String = Reflection.Assembly.GetExecutingAssembly.Location
        Dim app_Path As String = exe_Path.Substring(0, exe_Path.LastIndexOf("\"))

        Dim _openfiledialog As New OpenFileDialog

        _openfiledialog.Title = "Please select a PDF file"
        _openfiledialog.InitialDirectory = app_Path
        _openfiledialog.Filter = "PDF Files|*.PDF"

        Dim result As DialogResult = _openfiledialog.ShowDialog()

        If result = Windows.Forms.DialogResult.OK Then
            Dim _filename As String = _openfiledialog.FileName
            Dim _reader As New PdfReader(_filename)
            Dim _xfa As New XfaForm(_reader)
            Dim _xml As XmlNode = _xfa.DatasetsNode

            If IsNothing(_xml) = False Then
                Dim _document As XDocument = XDocument.Load(New XmlNodeReader(_xml))

                txt_Dataset.Text = _document.Document.FirstNode.ToString
            Else
                txt_Dataset.Text = "ten dokument nie zawiera Xfa"
            End If

            Dim _olo = 1
        End If
    End Sub
End Class
